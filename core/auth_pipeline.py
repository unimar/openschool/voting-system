from typing import Dict, Set
from decouple import config
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from jsonpath_ng import parse
from django.contrib.auth.models import Group, Permission

DEFAULT_APP_ROLES_PREFIX = "django."
DEFAULT_ROLES_PATH = "$.groups[*]"

FLAG_ROLES_MAP = {
    'is_staff': 'is_staff',
    'is_superuser': 'is_superuser',
}


@dataclass
class AuthorizationInfo:
    user_flags: Dict[str, bool]
    groups: Set[str]
    permissions: Set[str]

    def create_django_groups(self):
        ready_groups = Group.objects\
            .filter(name__in=self.groups)\
            .values_list('name', flat=True)
        new_groups = self.groups - set(ready_groups)
        Group.objects.bulk_create([
            Group(name=name)
            for name in new_groups
        ])

    def django_sync(self, django_user):

        for attr, value in self.user_flags.items():
            setattr(django_user, attr, value)

        permissions = Permission.objects.filter(codename__in=self.permissions)
        groups = Group.objects.filter(name__in=self.groups)

        django_user.groups.set(groups)
        django_user.user_permissions.set(permissions)
        django_user.save()
        return django_user


class AuthorizationInfoFactory(ABC):

    @abstractmethod
    def create(self, token_dat: Dict) -> AuthorizationInfo:
        raise NotImplementedError()


@dataclass
class Role2FlagAndGroupAuthorizationInfoFactory(AuthorizationInfoFactory):

    prefix: str | None = None
    roles_path: str = DEFAULT_ROLES_PATH
    flag_roles_map: Dict[str, str] = field(default_factory=lambda: dict(FLAG_ROLES_MAP))

    def create(self, token_data: Dict) -> AuthorizationInfo:
        jsonpath = parse(self.roles_path)
        roles = [str(match.value) for match in jsonpath.find(token_data)]
        app_prefix = self.prefix or ""
        app_roles = set([
            role.removeprefix(app_prefix)
            for role in roles
            if role.startswith(app_prefix)
        ])
        flags = {}
        for role, flag_key in self.flag_roles_map.items():
            flags[flag_key] = role in app_roles
        groups = set(app_roles - self.flag_roles_map.keys())
        return AuthorizationInfo(
            user_flags=flags,
            groups=groups,
            permissions={},
        )

    @classmethod
    def from_config(cls, APP_ROLES_PREFIX='APP_ROLES_PREFIX', ROLES_PATH='ROLES_PATH',):
        return cls(
            prefix=config(APP_ROLES_PREFIX, default=DEFAULT_APP_ROLES_PREFIX),
            roles_path=config(ROLES_PATH, default=DEFAULT_ROLES_PATH),
        )


DEFAULT = Role2FlagAndGroupAuthorizationInfoFactory.from_config()


def parse_authorization_info(backend, details, response, *args, **kwargs):
    authorization_info = DEFAULT.create(response)
    return {'authorization_info': authorization_info}


def create_authorization_info_groups(*args, authorization_info: AuthorizationInfo = None, **kwargs):
    if authorization_info is None:
        return
    authorization_info.create_django_groups()
    return {}


def sync_authorization_info(*args, user,  authorization_info: AuthorizationInfo = None, **kwargs):
    if authorization_info is None:
        return
    authorization_info.django_sync(user)
    return {}
