from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from django.contrib import admin


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    
    search_fields = [
        'name',
        'codename',
    ]
    
    list_display = [
        'name',
        'codename',
        'content_type',
    ]

    list_filter = [
        'content_type__app_label',
    ]


@admin.register(ContentType)
class ContentTypeAdmin(admin.ModelAdmin):
    list_display = [
        'app_label',
        'model',
    ]
    
    list_filter = [
        'app_label',
    ]
