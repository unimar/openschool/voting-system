from django.apps import AppConfig


class ModelGuardConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "model_guard"
