from django.contrib.auth.mixins import AccessMixin
from model_guard.models import filter_queryset


class ModelAccessMixin(AccessMixin):
    """Verify that the current user has all specified permissions."""

    required_permissions = None

    def get_app_label(self) -> str:
        return self.model._meta.app_label

    def get_required_permissions(self):
        """
        Override this method to override the permission_required attribute.
        Must return an iterable.
        """
        if self.required_permissions is None:
            raise Exception(
                f"{self.__class__.__name__} is missing the "
                f"required_permissions attribute. Define "
                f"{self.__class__.__name__}.required_permissions, or override "
                f"{self.__class__.__name__}.get_required_permissions()."
            )
        if isinstance(self.required_permissions, str):
            perms = (self.required_permissions,)
        else:
            perms = self.required_permissions
        app_label = self.get_app_label()
        perms = tuple(map(lambda it: f'{app_label}.{it}', perms))
        return perms

    def has_permission(self):
        """
        Override this method to customize the way permissions are checked.
        """
        perms = self.get_required_permissions()
        return self.request.user.has_perms(perms, None) or self.request.user.has_perms(perms, self.object)


class ListFilterObjectPermissionMixin(ModelAccessMixin):

    def get_queryset(self):
        perms = self.required_permissions
        if perms is None:
            return super().get_queryset()
        if isinstance(perms, str):
            perms = (perms,)
        return filter_queryset(super().get_queryset(), self.request.user, perms)


class CachedObjectMixin():

    object = None

    def get_object(self):
        if self.object is not None:
            return self.object
        self.object = super().get_object()
        return self.object


class ObjectPermissionMixin(CachedObjectMixin, ModelAccessMixin):

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.has_permission():
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class ObjectAuthorOrPermissionMixin(CachedObjectMixin, ModelAccessMixin):

    def get_queryset(self):
        return super().get_queryset().select_related('author')

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.author == self.request.user:
            return super().dispatch(request, *args, **kwargs)
        if not self.has_permission():
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
