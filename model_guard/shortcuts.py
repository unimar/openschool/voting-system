
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from model_guard.models import ModelGroupPermission, ModelUserPermission


def combine(a, b):
    for x in a:
        for y in b:
            yield x, y


def assign_permissions(object, permissions, groups=None, users=None):
    meta = object._meta
    groups = groups or []

    if isinstance(permissions, str):
        permissions = [permissions]

    permissions = Permission.objects.filter(
        content_type__app_label=meta.app_label,
        content_type__model=meta.model_name,
        codename__in=permissions
    )

    ModelGroupPermission.objects.bulk_create([
        ModelGroupPermission(group=group, feature=permission, target=object)
        for permission, group in combine(permissions, groups)
    ])

    users = users or []

    ModelUserPermission.objects.bulk_create([
        ModelUserPermission(user=user, feature=permission, target=object)
        for permission, user in combine(permissions, users)
    ])
