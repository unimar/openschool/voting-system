from logging import Logger
import logging
from typing import Any
from dataclasses import dataclass
from django.db import models
from functools import reduce
from operator import and_
from django.db.models import Q
from django.db.models import Value, CharField
from django.db.models.functions import Concat
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

__logger__: Logger = logging.getLogger(__name__)


def filter_queryset(queryset, user, perms):
    if user.is_anonymous:
        return queryset.filter(pk__in=[])
    if user.is_superuser:
        return queryset
    meta = queryset.model._meta
    permission_queries = []
    fixed_perms = []
    for perm in perms:
        if '.' in perm:
            app_label, codename = perm.split('.', 1)
            fixed_perms.append(perm)
            if app_label != meta.app_label:
                raise Exception(f'App label of {perm} and {queryset.model} doesn\'t match')
        else:
            codename = perm
            fixed_perms.append(meta.app_label + '.' + perm)
        permission_queries.append(Q(
            enabled=True,
            target_type__app_label=meta.app_label,
            target_type__model=meta.model_name,
            feature__codename=codename
        ))
    if user.has_perms(fixed_perms, None):
        return queryset
    permission_query = reduce(and_, permission_queries)
    user_objects = ModelUserPermission.objects.filter(
        permission_query,
        user=user,
    ).values_list('target_pk', flat=True)
    group_objects = ModelGroupPermission.objects.filter(
        permission_query,
        group__user=user,
    ).values_list('target_pk', flat=True)
    return queryset.filter(pk__in=user_objects.union(group_objects))


def get_all_permissions(user, obj=None):
    return set(get_peer_user_permissions(user, obj)) | set(get_peer_group_permissions(user, obj))


def has_permissions(user, perms, obj=None):
    if user.is_anonymous or not user.is_active:
        return False
    if user.is_superuser or user.has_perms(perms):
        return True
    if obj is None:
        return False
    meta = obj._meta
    permission_queries = []
    for perm in perms:
        if '.' in perm:
            app_label, codename = perm.split('.', 1)
            if meta.app_label != app_label:
                raise Exception(f'App label of {perm} and {obj} doesn\'t match')
        else:
            codename = perm
        permission_queries.append(Q(
            enabled=True,
            target_type__app_label=meta.app_label,
            target_type__model=meta.model_name,
            feature__codename=codename
        ))
    app_label = obj._meta.app_label
    model_name = obj._meta.model_name
    permission_query = reduce(and_, permission_queries)
    return ModelUserPermission.objects.filter(
        permission_query,
        user=user,
        feature__content_type__app_label=app_label,
        feature__content_type__model=model_name,
        target_pk=obj.pk
    ).exists() or ModelGroupPermission.objects.filter(
        permission_query,
        group__user=user,
        feature__content_type__app_label=app_label,
        feature__content_type__model=model_name,
        target_pk=obj.pk
    ).exists()


def get_peer_user_permissions(user, obj):
    if obj is None:
        return set()
    meta = obj._meta
    app_label = meta.app_label
    model_name = meta.model_name
    user_pk = None if user.is_anonymous else user.pk
    return ModelUserPermission.objects.filter(
        enabled=True,
        user__pk=user_pk,
        target_type__app_label=app_label,
        target_type__model=model_name,
        target_pk=obj.pk,
    ).annotate(
        feature_code=Concat(
            'target_type__app_label',
            Value('.'),
            'feature__codename',
            output_field=CharField()
        )).values_list('feature_code', flat=True)


def get_peer_group_permissions(user, obj):
    if obj is None:
        return {}
    meta = obj._meta
    app_label = meta.app_label
    model_name = meta.model_name
    user_pk = None if user.is_anonymous else user.pk
    return ModelGroupPermission.objects.filter(
        enabled=True,
        group__user=user_pk,
        target_type__app_label=app_label,
        target_type__model=model_name,
        target_pk=obj.pk,
    ).annotate(
        feature_code=Concat(
            'target_type__app_label',
            Value('.'),
            'feature__codename',
            output_field=CharField()
        )).values_list('feature_code', flat=True)


class ModelUserPermission(models.Model):

    enabled = models.BooleanField(
        default=True,
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    feature = models.ForeignKey(
        'auth.Permission',
        on_delete=models.CASCADE,
    )

    target_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
    )

    target_pk = models.PositiveIntegerField()

    target = GenericForeignKey(
        'target_type',
        'target_pk'
    )

    def __str__(self) -> str:
        return f'{self.user}:{self.feature}:{self.target}'


class ModelGroupPermission(models.Model):

    enabled = models.BooleanField(
        default=True,
    )

    group = models.ForeignKey(
        'auth.Group',
        on_delete=models.CASCADE,
    )

    feature = models.ForeignKey(
        'auth.Permission',
        on_delete=models.CASCADE,
    )

    target_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
    )

    target_pk = models.PositiveIntegerField()

    target = GenericForeignKey(
        'target_type',
        'target_pk'
    )

    def __str__(self) -> str:
        return f'{self.group}:{self.feature}:{self.target}'
