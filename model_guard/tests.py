from django.test import TestCase
from model_guard.models import has_permissions, ModelUserPermission
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission

# Create your tests here.


class Test(TestCase):

    def test_has_permission_in_all_objects(self):
        User = get_user_model()
        user = User.objects.create_user(username='user')
        obj = User.objects.create_user(username='test')
        user.user_permissions.add(Permission.objects.get(codename='view_user'))
        user = User.objects.get(username='user')
        self.assertTrue(has_permissions(user, ['auth.view_user']))
        self.assertTrue(has_permissions(user, ['auth.view_user'], obj))

    def test_has_permission_in_specific_object(self):
        User = get_user_model()
        user = User.objects.create_user(username='user')
        obj = User.objects.create_user(username='test')
        self.assertFalse(has_permissions(user, ['auth.view_user'], obj))
        ModelUserPermission.objects.create(
            user=user,
            feature=Permission.objects.get(
                content_type__app_label='auth',
                codename='view_user'
            ),
            target=obj,
        )
        self.assertTrue(has_permissions(user, ['auth.view_user'], obj))
