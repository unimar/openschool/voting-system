from django.contrib import admin
from model_guard.models import *
# Register your models here.


@admin.register(ModelUserPermission)
class ModelUserPermissionAdmin(admin.ModelAdmin):
    list_display = [
        '__str__',
        'feature',
        'user',
        'target_type',
        'target_pk',
        'enabled',
    ]

    list_filter = [
        'target_type',
        'enabled',
    ]


@admin.register(ModelGroupPermission)
class ModelGroupPermissionAdmin(admin.ModelAdmin):
    list_display = [
        '__str__',
        'feature',
        'group',
        'target_type',
        'target_pk',
        'enabled',
    ]

    list_filter = [
        'target_type',
        'enabled',
    ]
