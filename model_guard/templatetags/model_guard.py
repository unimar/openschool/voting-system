from typing import Any
from functools import partial
from django import template
from dataclasses import dataclass
from model_guard.models import has_permissions

register = template.Library()


@dataclass
class UserLazyPerms:
    user: Any

    def can(self, permission, object) -> bool:
        return has_permissions(self.user, [permission], object)

    def can_or_author(self, permission, object) -> bool:
        author = getattr(object, 'author', None)
        return author == self.user or self.can(permission, object)


@register.filter(name="can")
def can(user, permission: str):
    perms = UserLazyPerms(user)
    return partial(perms.can, permission)


@register.filter(name="can_or_author")
def can_or_author(user, permission: str):
    perms = UserLazyPerms(user)
    return partial(perms.can_or_author, permission)


@register.filter(name="on")
def on(partial_fn, obj):
    return partial_fn(obj)
