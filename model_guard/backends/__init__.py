from django.db.models import Q
from django.contrib.contenttypes.models import ContentType
from model_guard.models import get_all_permissions, has_permissions


def get_content_type(obj):
    return ContentType.objects.get_for_model(obj)


class ObjectPermissionBackend:
    supports_object_permissions = True
    supports_anonymous_user = True
    supports_inactive_user = True

    def authenticate(self, request, username=None, password=None):
        return None

    def has_perm(self, user_obj, perm, obj=None):
        """
        Returns ``True`` if given ``user_obj`` has ``perm`` for ``obj``. If no
        ``obj`` is given, ``False`` is returned.

        .. note::

           Remember, that if user is not *active*, all checks would return
           ``False``.

        Main difference between Django's ``ModelBackend`` is that we can pass
        ``obj`` instance here and ``perm`` doesn't have to contain
        ``app_label`` as it can be retrieved from given ``obj``.

        **Inactive user support**

        If user is authenticated but inactive at the same time, all checks
        always returns ``False``.
        """
        if obj is None:
            return False

        if '.' in perm:
            app_label, _ = perm.split('.', 1)
            if app_label != obj._meta.app_label:
                # Check the content_type app_label when permission
                # and obj app labels don't match.
                ctype = get_content_type(obj)
                if app_label != ctype.app_label:
                    raise Exception("Passed perm has app label of '%s' while "
                                    "given obj has app label '%s' and given obj"
                                    "content_type has app label '%s'" %
                                    (app_label, obj._meta.app_label, ctype.app_label))
        return has_permissions(user_obj, [perm], obj)

    def get_all_permissions(self, user_obj, obj=None):
        """
        Returns a set of permission strings that the given ``user_obj`` has for ``obj``
        """
        if obj is None:
            return set()

        return get_all_permissions(user_obj, obj)
