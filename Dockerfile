FROM python:3.12.2-alpine as builder
RUN mkdir /home/app
WORKDIR /home/app
COPY Pipfile Pipfile
COPY Pipfile.lock Pipfile.lock
ENV PIPENV_VENV_IN_PROJECT=1
RUN python -m pip install pipenv
RUN pipenv install
FROM python:3.12-alpine as runner
RUN adduser -D app
RUN addgroup app root
WORKDIR /home/app
COPY --chown=app:root . .
COPY --from=builder --chown=app:root /home/app/.venv /home/app/.venv
USER app
ENTRYPOINT [ "/home/app/entrypoint.sh" ]
CMD [ "ash" ]
FROM runner as migration
ENV DATABASE_URL=sqlite:////tmp/data/db.sqlite3
RUN mkdir -p -m 0771 /tmp/data
CMD ["python", "-O", "manage.py", "migrate"]
FROM runner as web
EXPOSE 5000
ENV DEBUG=False
ENV DATABASE_URL=sqlite:////tmp/data/db.sqlite3
RUN mkdir -p -m 0771 /tmp/data
RUN [".venv/bin/python", "manage.py", "collectstatic", "--no-input"]
CMD ["python", "-O", "-m","uvicorn", "core.asgi:application", "--proxy-headers", "--host=0.0.0.0", "--port=5000"]
