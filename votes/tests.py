from django.test import TestCase
from votes.models import *
from django.contrib.auth import get_user_model


class VoteTest(TestCase):

    def test_count_votes(self):
        User = get_user_model()
        john = User.objects.create_user(username='John', password='pass123')
        poll = Poll.objects.create(title='A')
        options = Option.objects.bulk_create([
            Option(poll=poll, title='1.'),
            Option(poll=poll, title='2.'),
            Option(poll=poll, title='3.'),
            Option(poll=poll, title='4.'),
        ])
        for index, option in enumerate(options):
            Vote.objects.create(
                author=john,
                option=option,
                value=index
            )
        for index, option in enumerate(options):
            option = Option.objects.with_sum_votes_value().with_count_votes().get(pk=option.pk)
            self.assertEqual(option.sum_votes_value, index)
            self.assertEqual(option.count_votes, 1)
