from typing import Any
from django.contrib import admin
from django.db.models.query import QuerySet
from django.http import HttpRequest
from votes.models import *


class InlineOption(admin.TabularInline):
    model = Option


@admin.register(Poll)
class PollAdmin(admin.ModelAdmin):

    @admin.display(
        boolean=True,
        ordering='open_to_vote',
    )
    def open_to_vote(self, obj):
        return obj.open_to_vote

    @admin.display(
        boolean=True,
        ordering='open_to_suggest',
    )
    def open_to_suggest(self, obj):
        return obj.open_to_suggest

    @admin.action(description='Enable')
    def enable(self, request, queryset):
        queryset.update(active=True)

    @admin.action(description='Disable')
    def disable(self, request, queryset):
        queryset.update(active=False)

    search_fields = [
        'title',
        'description',
    ]

    list_display = [
        'title',
        'active',
        'real_time',
        'open_to_vote',
        'open_to_suggest',
        'suggest_after',
        'suggest_until',
        'vote_after',
        'vote_until',
    ]

    list_filter = [
        'active',
        'vote_until',
        'suggest_until',
    ]

    inlines = [InlineOption]

    actions = [enable, disable]


@admin.register(Option)
class OptionAdmin(admin.ModelAdmin):

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        return super().get_queryset(request)\
            .with_sum_votes_value()\
            .with_count_votes()

    search_fields = [
        'title',
        'description',
    ]

    list_display = [
        'title',
        'poll',
        'sum_votes_value',
        'count_votes',
        'created_at',
        'updated_at',
    ]

    @admin.display(
        ordering='sum_votes_value',
    )
    def sum_votes_value(self, obj):
        return obj.sum_votes_value

    @admin.display(
        ordering='count_votes',
    )
    def count_votes(self, obj):
        return obj.count_votes


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):

    list_display = [
        'author',
        'option',
        'value',
        'created_at',
        'updated_at',
    ]
