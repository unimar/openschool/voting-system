from typing import Any
from django.db.models.base import Model as Model
from django.forms.models import model_to_dict
from django import forms
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect
from votes.models import *
from django import forms
from django.contrib.auth.models import Group
from django.forms import ModelForm, Form
from django.shortcuts import resolve_url
from django.views.generic import ListView, UpdateView, DetailView
from django.views.generic.edit import UpdateView, DeleteView
from model_guard.views import ObjectPermissionMixin, ObjectAuthorOrPermissionMixin, ListFilterObjectPermissionMixin


class PollForm(ModelForm):
    class Meta:
        model = Poll
        exclude = ('author',)


class PollChangeView(ObjectPermissionMixin, UpdateView):
    required_permissions = 'change_poll'
    template_name = 'form.html'
    model = Poll
    form_class = PollForm

    def get_success_url(self) -> str:
        return resolve_url('votes:poll-change', pk=self.object.pk)


class PollAssignForm(ModelForm):

    groups = forms.ModelMultipleChoiceField(
        queryset=Group.objects.all(),
    )

    class Meta:
        model = Poll
        fields = []


class PollAssignView(ObjectPermissionMixin, UpdateView):
    required_permissions = 'change_poll'
    template_name = 'form.html'
    model = Poll
    form_class = PollAssignForm

    def get_success_url(self) -> str:
        return resolve_url('votes:poll-list')

    def form_valid(self, form):
        groups = Group.objects.filter(pk__in=form.data['groups'])
        self.object.assign_permissions(
            permissions=('view_poll', 'poll_vote', 'poll_suggest'),
            groups=groups,
        )
        return super().form_valid(form)


class PollResultView(ObjectPermissionMixin, DetailView):
    required_permissions = 'view_poll'
    template_name = 'poll/results.html'
    model = Poll
    queryset = Poll.objects.filter(open_to_follow=True)


class PollListView(ListFilterObjectPermissionMixin, ListView):
    required_permissions = 'view_poll'
    template_name = 'poll/list.html'
    model = Poll
    queryset = Poll.objects.filter(active=True)


class PollSuggestForm(ModelForm):

    class Meta:
        model = Option
        fields = [
            'title',
            'description',
        ]


class PollVoteForm(ModelForm):

    value = forms.FloatField(
        min_value=-1,
        max_value=+1,
        widget=forms.RadioSelect(choices=(
            (-1., '-1'),
            (0., '0'),
            (+1., '+1'),
        )),
        initial=0.,
    )

    class Meta:
        model = Vote
        fields = [
            'value',
        ]

    def save(self, author, option, commit: bool = True) -> Any:
        instance, _ = Vote.objects.update_or_create(
            defaults=self.cleaned_data,
            author=author,
            option=option,
        )
        return instance


class PollSuggestView(ObjectPermissionMixin, UpdateView):
    required_permissions = 'poll_suggest'
    template_name = 'poll/suggest.html'
    model = Poll
    queryset = Poll.objects.filter(open_to_suggest=True)
    form_class = PollSuggestForm

    def get_success_url(self) -> str:
        return resolve_url('votes:poll-suggest', pk=self.object.pk)

    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = None
        return kwargs

    def form_valid(self, form):
        form.instance.poll = self.object
        form.instance.author = self.request.user
        form.save()
        return redirect(self.get_success_url())


class PollVoteView(ObjectPermissionMixin, UpdateView):
    required_permissions = 'poll_vote'
    template_name = 'poll/vote.html'
    model = Poll
    queryset = Poll.objects.filter(open_to_vote=True)
    fields = []

    def get_success_url(self) -> str:
        if self.object.open_to_follow:
            return resolve_url('votes:poll-results', pk=self.object.pk)
        else:
            return resolve_url('votes:poll-list')

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        if 'forms' not in kwargs:
            kwargs['forms'] = self.get_forms()
        return super().get_context_data(**kwargs)

    def form_valid(self, form, option):
        return form.save(author=self.request.user, option=option)

    def get_forms(self, data=None):
        for option in self.object.option_set.all():
            try:
                vote = Vote.objects.get(author=self.request.user, option=option)
            except Vote.DoesNotExist:
                vote = Vote(
                    author=self.request.user,
                    option=option,
                    value=0.,
                )
            yield option, PollVoteForm(
                data,
                prefix=f'option_{option.pk}',
                initial=model_to_dict(vote),
                instance=vote,
            )

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        forms = list(self.get_forms(request.POST))
        error = False
        for option, form in forms:
            if form.is_valid():
                self.form_valid(form, option)
            else:
                error = True
        if error:
            return self.render_to_response(self.get_context_data(forms=forms))
        return redirect(self.get_success_url())


class OptionForm(ModelForm):
    class Meta:
        model = Option
        exclude = ('author',)


class OptionChangeView(ObjectAuthorOrPermissionMixin, UpdateView):
    required_permissions = 'change_option'
    template_name = 'form.html'
    model = Option
    form_class = OptionForm
    queryset = Option.objects.filter(
        poll__in=Poll.objects.filter(open_to_suggest=True),
        vote__isnull=True,
    )

    def get_success_url(self) -> str:
        return resolve_url('votes:poll-suggest', pk=self.object.poll.pk)


class OptionDeleteView(ObjectAuthorOrPermissionMixin, DeleteView):
    required_permissions = 'delete_option'
    template_name = 'form.html'
    model = Option
    queryset = Option.objects.filter(
        poll__in=Poll.objects.filter(open_to_suggest=True),
        vote__isnull=True,
    )

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["delete"] = True
        return context

    def get_success_url(self) -> str:
        return resolve_url('votes:poll-suggest', pk=self.object.poll.pk)
