from django.db import models
from django.db.models import When, Case, Value
from django.db.models.functions import Now
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from model_guard.shortcuts import assign_permissions


class PollQueryset(models.QuerySet):

    def with_open_to_vote(self):
        return self.annotate(
            open_to_vote=Case(
                When(vote_until=None, vote_after=None, then=Value(True)),
                When(vote_after__lte=Now(), vote_until=None, then=Value(True)),
                When(vote_after=None, vote_until__gte=Now(), then=Value(True)),
                When(vote_after__lte=Now(), vote_until__gte=Now(), then=Value(True)),
                default=Value(False),
                output_field=models.BooleanField('open_to_vote'),
            )
        )

    def with_open_to_suggest(self):
        return self.annotate(
            open_to_suggest=Case(
                When(suggest_until=None, suggest_after=None, then=Value(True)),
                When(suggest_after__lte=Now(), suggest_until=None, then=Value(True)),
                When(suggest_after=None, suggest_until__gte=Now(), then=Value(True)),
                When(suggest_after__lte=Now(), suggest_until__gte=Now(), then=Value(True)),
                default=Value(False),
                output_field=models.BooleanField('open_to_suggest'),
            )
        )

    def with_open_to_follow(self):
        return self.annotate(
            open_to_follow=Case(
                When(real_time=True, then=Value(True)),
                When(vote_until__gte=Now(), then=Value(False)),
                When(open_to_vote=True, then=Value(False)),
                default=Value(True),
                output_field=models.BooleanField('open_to_follow'),
            )
        )

    def with_open_to(self):
        return self\
            .with_open_to_suggest() \
            .with_open_to_vote() \
            .with_open_to_follow()


class PollManager(models.Manager.from_queryset(PollQueryset)):

    def get_queryset(self) -> models.QuerySet:
        return super().get_queryset().with_open_to()


class Poll(models.Model):

    objects = PollManager()

    class Meta:
        ordering = [
            'title',
        ]
        permissions = [
            ("poll_vote", "Pode votar"),
            ("poll_suggest", "Pode Sugerir"),
        ]

    active = models.BooleanField(
        default=True,
    )

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    title = models.CharField(
        max_length=256,
    )

    description = models.TextField(
        null=True,
        blank=True,
    )

    real_time = models.BooleanField(
        default=False,
    )

    suggest_after = models.DateTimeField(
        null=True,
        default=None,
        blank=True,
    )

    suggest_until = models.DateTimeField(
        null=True,
        default=None,
        blank=True,
    )

    vote_after = models.DateTimeField(
        null=True,
        default=None,
        blank=True,
    )

    vote_until = models.DateTimeField(
        null=True,
        default=None,
        blank=True,
    )

    def __str__(self) -> str:
        return self.title

    def get_sorted_options(self):
        return self.option_set\
            .with_sum_votes_value()\
            .with_count_votes()\
            .order_by('-sum_votes_value')

    assign_permissions = assign_permissions


class OptionQuerySet(models.QuerySet):

    def with_sum_votes_value(self):
        return self.annotate(
            sum_votes_value=models.Sum('vote__value', default=0.)
        )

    def with_count_votes(self):
        return self.annotate(
            count_votes=models.Count('vote__pk')
        )


class Option(models.Model):
    
    objects = OptionQuerySet.as_manager()
    
    class Meta:
        ordering = [
            'title',
        ]

    created_at = models.DateTimeField(
        auto_now_add=True,
    )

    updated_at = models.DateTimeField(
        auto_now=True,
    )

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    poll = models.ForeignKey(
        Poll,
        on_delete=models.CASCADE,
    )

    title = models.CharField(
        max_length=256,
    )

    description = models.TextField(
        null=True,
        blank=True,
    )
    
    def can_delete(self):
        return self.poll.open_to_suggest and self.count_votes == 0
    
    def can_change(self):
        return self.poll.open_to_suggest and self.count_votes == 0

    def __str__(self) -> str:
        return self.title


class Vote(models.Model):

    class Meta:
        unique_together = [
            ('author', 'option',)
        ]

    created_at = models.DateTimeField(
        auto_now_add=True,
    )

    updated_at = models.DateTimeField(
        auto_now=True,
    )

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    option = models.ForeignKey(
        Option,
        on_delete=models.CASCADE
    )

    value = models.FloatField(
        default=1.,
    )
