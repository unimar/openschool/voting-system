from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import path, include
from votes import views

app_name = 'votes'

urlpatterns = [
    path("logout/", LogoutView.as_view(next_page='/'), name='logout'),
    path('poll/', views.PollListView.as_view(), name='poll-list'),
    path('poll/<pk>/change/', views.PollChangeView.as_view(), name='poll-change'),
    path('poll/<pk>/assign/', views.PollAssignView.as_view(), name='poll-assign'),
    path('poll/<pk>/vote/', views.PollVoteView.as_view(), name='poll-vote'),
    path('poll/<pk>/suggest/', views.PollSuggestView.as_view(), name='poll-suggest'),
    path('poll/<pk>/results/', views.PollResultView.as_view(), name='poll-results'),
    path('option/<pk>/change/', views.OptionChangeView.as_view(), name='option-change'),
    path('option/<pk>/delete/', views.OptionDeleteView.as_view(), name='option-delete'),
]
