#!/bin/ash -e

for secret in $(ls -1 /run/secrets)
do
	export ${secret}=$(cat /run/secrets/${secret})
done


source .venv/bin/activate

if [[ $MIGRATE -eq 1 ]]; then

    python manage.py migrate

fi

exec "$@"
